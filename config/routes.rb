Rails.application.routes.draw do
  resources :articles
  root "articles#home"
  get "/accueil", to: "articles#home"
  resources :profiles
end
##
##root GET    /                                                                                                 articles#index
##articles GET    /articles(.:format)                                                                               articles#index
##         POST   /articles(.:format)                                                                               articles#create
##new_article GET    /articles/new(.:format)                                                                           articles#new
##edit_article GET    /articles/:id/edit(.:format)                                                                      articles#edit
## article GET    /articles/:id(.:format)                                                                           articles#show
##         PATCH  /articles/:id(.:format)                                                                           articles#update
##         PUT    /articles/:id(.:format)                                                                           articles#update
##         DELETE /articles/:id(.:format)                                                                           articles#destroy
##profiles GET    /profiles(.:format)                                                                               profiles#index
##         POST   /profiles(.:format)                                                                               profiles#create
##new_profile GET    /profiles/new(.:format)                                                                           profiles#new
##edit_profile GET    /profiles/:id/edit(.:format)                                                                      profiles#edit
## profile GET    /profiles/:id(.:format)                                                                           profiles#show
##         PATCH  /profiles/:id(.:format)                                                                           profiles#update
##         PUT    /profiles/:id(.:format)                                                                           profiles#update
##         DELETE /profiles/:id(.:format)
