class ProfilesController < ApplicationController
  def index
    @profiles = Profile.all
  end

  def show
    @profile = Profile.find(params[:id])
    logger.info "GET..."
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new(profile_params)

    if @profile.save
      redirect_to profile_path(@profile), :notice => "User successfully added."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @profile = Profile.find(params[:id])
  end

  def update
    @profile = Profile.find(params[:id])

    if @profile.update(profile_params)
      redirect_to profile_path(@profile), :notice => "User successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @profile = Profile.find(params[:id])
    @profile.destroy
    logger.info "Processing the request..."
    redirect_to profiles_path, status: :see_other
  end

  private

  def profile_params
    params.require(:profile).permit(:prenom, :nom, :adresse, :age, :photo)
  end
end
